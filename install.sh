#!/usr/bin/bash

## GLOBAL VARS
MINIKUBE_MEMORY="16384"
MINIKUBE_CPU="4"
MINIKUBE_DRIVER="kvm2"
MINIKUBE_NAME="my-istio-mini"
ISTIO_VERSION=1.12.0 
TARGET_ARCH=x86_64
ISTIO_PROFILE="default"
DEFAULT_NAMESPACE="default"

# Creating minikube cluster

#Check if cluster exists
if minikube profile list | grep -q ${MINIKUBE_NAME} ; then
  echo "Minikube cluster (${MINIKUBE_NAME}) exists"
else
  minikube start --cpus=${MINIKUBE_CPU} --memory=${MINIKUBE_MEMORY} --driver=${MINIKUBE_DRIVER} --addons=metric-server,dashboard -p ${MINIKUBE_NAME}
fi

# Install istioctl locally
curl -L https://istio.io/downloadIstio | ISTIO_VERSION=${ISTIO_VERSION} TARGET_ARCH=${TARGET_ARCH} sh -

chmod +x istio-${ISTIO_VERSION}/bin/istioctl

./istio-${ISTIO_VERSION}/bin/istioctl x precheck
if [ $? -ne 0 ]; then
  echo "ERROR: istioctl precheck failed"
else
  echo "INFO: Installating istio with ${ISTIO_PROFILE} profile"
  ./istio-${ISTIO_VERSION}/bin/istioctl install --set profile=${ISTIO_PROFILE} -y
fi


# Create self signed certificate for example manifest
openssl req -newkey rsa:2048 -x509 -nodes -keyout server.key -new -out server.crt -subj /CN=hello-app.example.com -sha256

# Create kubernetes TLS certificate secret
kubectl create -n istio-system secret tls test-certificate --key=server.key --cert=server.crt

# Inject istio label to the namespace
kubectl label namespace ${DEFAULT_NAMESPACE} istio-injection=enabled

# Apply test manifests
kubectl apply -f manifests/ -n ${DEFAULT_NAMESPACE}

echo -e "\n\n\n\n\n\n---------------------------------------------"
echo "Please tunnel minikube with below commnand in a seperate terminal"
echo "minikube tunnel -p ${MINIKUBE_NAME}"
echo -e "---------------------------------------------"
LOADBALANCER_IP=$(kubectl get svc -n istio-system istio-ingressgateway | tail -1 | awk '{print $4}')
echo "Please add hello-app.example.com to your hosts file with ${LOADBALANCER_IP} IP address"
echo -e "---------------------------------------------"
echo "Please open a browser with https://hello-app.example.com"
echo -e "---------------------------------------------"
rm -rf istio-${ISTIO_VERSION}