# Minikube Istio

## Getting started
The aim of the project is to start a minikube cluster with kvm2 driver and install the latest `istioctl` command line utility with `default` istio profile

## Installation Steps
```bash
git clone https://gitlab.com/yasarlaro/minikube-istio.git
cd minikube-istio
chmod +x install.sh && ./install.sh
```

## Default Variables
```bash
MINIKUBE_MEMORY="16384"
MINIKUBE_CPU="4"
MINIKUBE_DRIVER="kvm2"
MINIKUBE_NAME="my-istio-mini"
ISTIO_VERSION=1.12.0 
TARGET_ARCH=x86_64
ISTIO_PROFILE="default"
DEFAULT_NAMESPACE="default"
```